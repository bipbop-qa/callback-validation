/* eslint-disable no-console */
require('dotenv').config();

const uuidv1 = require('uuid/v1');
const humanInterval = require('human-interval');

const testName = require('./lib/test-name');

const {
  testData,
  testQuery,
  testTimeout,
  testDegraded,
} = require('./lib/test-parameters');

const statusPage = require('./lib/status-page-instance');
const findOrCreateComponent = require('./lib/find-or-create-component');
const updateComponentState = require('./lib/update-component-state');
const outageLevel = require('./lib/outage-level');
const push = require('./lib/push-instance');
const test = require('./lib/test');

async function main() {
  let exitCode = 0;
  let status = 'operational';
  const label = uuidv1();

  const component = await findOrCreateComponent(testName, statusPage);
  if (component && component.status === 'under_maintenance') {
    console.error('A fonte se encontra em manutenção, não é possível prosseguir o teste');
    process.exit(1);
    return;
  }

  /* possui apenas 5 min para definir um timeout */
  const dpTimeout = setTimeout(() => {
    status = 'degraded_performance';
  }, humanInterval(testDegraded));

  console.log('Informações do Teste', {
    testQuery,
    testData,
    testTimeout,
    label,
  });

  try {
    const response = await test(testQuery, testData, testTimeout, push, label);
    console.log('Informações do PUSH', response);
    clearTimeout(dpTimeout);
  } catch (e) {
    clearTimeout(dpTimeout);
    exitCode = 1;
    status = outageLevel(component);
    console.error(e.message);
  }

  await updateComponentState(component, status, statusPage);
  process.exit(exitCode);
}

main();
