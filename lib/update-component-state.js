const defaultStatusPage = require('./status-page-instance');
const findOrCreateComponent = require('./find-or-create-component');
const outageLevel = require('./outage-level');

async function updateComponentState(component, status, statusPage = defaultStatusPage) {
  if (!statusPage) return;
  if (!component) return;
  if (component.cached) {
    await updateComponentState(await findOrCreateComponent(
      component.name,
      statusPage,
      false,
    ), await outageLevel(component, status), statusPage);
    return;
  }
  if (component.status === status) return;
  await statusPage.updateComponentState(component.id, status, component.name);
}

module.exports = async (...args) => {
  try {
    await updateComponentState(...args);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(`Não foi possível atualizar o componente de teste: ${e.message}`);
  }
};
