const { WebService, pushParameters, get } = require('bipbop-webservice');
const BipbopWebSocket = require('bipbop-websocket');
const humanInterval = require('human-interval');
const onetime = require('onetime');

const { testData, testQuery, testTimeout } = require('./test-parameters');

const push = require('./push-instance');

module.exports = (
  query = testQuery,
  parameterData = testData,
  timeout = testTimeout,
  pushInstance = push,
  label = null,
) => {
  let id = null;
  return new Promise((resolve, reject) => {
    let webSocket;
    let error;

    const instantiateError = (exception) => new Error(`PushID: ${id}, Query: ${query}, Data: ${parameterData}, Exception: ${exception}`);

    const errorTimeout = setTimeout(() => {
      if (webSocket) webSocket.close();
      reject(error || instantiateError('O intervalo máximo foi cumprido.'));
    }, humanInterval(timeout));

    webSocket = new BipbopWebSocket(push.ws.apiKey, (payload) => {
      /* O PUSH não rodou ainda */
      if (!id) return;

      /* verifica se é um update de PUSH */
      const { method, data } = payload;
      if (method !== 'pushUpdate') return;

      /* verifica se é o ID que desejamos */
      const { pushObject: { _id: receivedPushId } } = data;
      if (receivedPushId !== id) return;

      /* busca por uma exceção */
      const exception = get(WebService.parse(get(data, 'document.data')) || {
        BPQL: { header: { exception: 'Não há um documento disponível nesta memória' } },
      }, 'BPQL.header.exception');

      if (exception) {
        error = instantiateError(exception);
        return;
      }

      /* finaliza o teste */
      clearTimeout(errorTimeout);
      webSocket.close();
      resolve({ id });
    }, onetime(() => pushInstance.insertJob({
      [pushParameters.query]: query,
      [pushParameters.webSocketDeliver]: 'true',
      [pushParameters.expire]: ((Date.now() + humanInterval(timeout)) / 1000).toString(),
      [pushParameters.maxVersion]: (1).toString(),
      [pushParameters.tryIn]: (humanInterval('10 seconds') / 1000).toString(),
      [pushParameters.label]: label,
      data: parameterData,
    })
      .then((response) => response.text())
      .then((text) => WebService.parse(text))
      .then(({ BPQL }) => BPQL)
      .then(({ body }) => body)
      .then(({ id: pushId }) => {
        if (!pushId) throw new Error('Não foi recebido o ID do PUSH');
        id = pushId;
      })));
  });
};
