const { testQuery: defaultTestQuery, testData: defaultTestData } = require('./test-parameters');

const {
  TEST_NAME: testName,
  testName: testNameAlternative,
} = process.env;

/**
 * Cria um nome para o teste baseado no ambiente caso um não esteja configurado
 * @param {string} testQuery Query a ser utilizada no serviço principal
 * @param {string} testData Sub-query a ser utilizada num microserviço da constelação
 * @returns {null,string} Nome do teste
 */
function createTestName(
  testQuery = defaultTestQuery,
  testData = defaultTestData,
) {
  const testUse = testData || testQuery;
  if (!testUse) return null;
  const [database, table] = testUse.match(/'([^']+)'/g).map((value) => value.replace(/'/g, ''));
  const keyName = `DB_${database.toUpperCase().trim('\'')}_${table.toUpperCase().trim('\'')}`;
  return process.env[keyName] || null;
}

module.exports = testName || testNameAlternative || createTestName();
