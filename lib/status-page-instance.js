const StatusPage = require('statuspage');
const testName = require('./test-name');

const {
  K8S_SECRET_STATUS_PAGE_APIKEY: statusPageApiKeySecret,
  STATUS_PAGE_ID: statusPageId,
  statusPageApiKey,
  statuspagePageId,
} = process.env;

const statusPage = testName
    && (statuspagePageId || statusPageId)
    && (statusPageApiKey || statusPageApiKeySecret)
  ? new StatusPage(statusPageApiKey, statuspagePageId || statusPageId)
  : null;

module.exports = statusPage;
