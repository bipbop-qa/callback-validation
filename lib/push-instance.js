const { WebService, Push } = require('bipbop-webservice');

const { apiKey: apiKeyAlternative, K8S_SECRET_BIPBOP_APIKEY: apiKey } = process.env;

const push = new Push(new WebService(apiKey || apiKeyAlternative));

module.exports = push;
