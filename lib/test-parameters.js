const {
  TEST_QUERY: testQuery,
  TEST_DATA: testData,
  testQuery: testQueryAlternative,
  testData: testDataAlternative,
  TEST_TIMEOUT: testTimeout,
  TEST_DEGRADED: testDegraded,

} = process.env;

const defaultParameter = "SELECT FROM 'INFO'.'INFO'";
const defaultTestTimeout = '15 minutes';
const defaultTestDegraded = '5 minutes';

exports.testData = testData
  || testDataAlternative
  || defaultParameter;

exports.testQuery = testQuery
  || testQueryAlternative
  || defaultParameter;

exports.testTimeout = testTimeout
  || defaultTestTimeout;

exports.testDegraded = testDegraded
  || defaultTestDegraded;
