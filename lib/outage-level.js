async function outageLevel(component, lastStatus = null) {
  if (lastStatus === 'operational') return 'operational';
  if (!component) return 'major_outage';
  if (component.status === 'under_maintenance') return 'under_maintenance';
  if (component.cached) return 'major_outage';
  if (component.status === 'major_outage' || component.status === 'partial_outage') return 'major_outage';
  return 'partial_outage';
}

module.exports = outageLevel;
