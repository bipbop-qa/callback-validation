const defaultTestName = require('./test-name');
const componentsCache = require('../components-cache.json');

function findComponentCache(testName = defaultTestName) {
  const data = componentsCache.find(({ name }) => name === testName);
  if (!data) return null;
  return Object.assign(data, { cached: true });
}

module.exports = findComponentCache;
