const fs = require('fs');

const defaultTestName = require('./test-name');
const defaultStatusPage = require('./status-page-instance');
const findComponentCache = require('./find-component-cache');

/**
 * Encontra um componente de teste com o nome ou o cria
 * @param {string} testName Nome do teste a ser realizado
 * @param {StatusPage} statusPage Instância do statuspage
 * @param {boolean} useCache Devemos utilizar o cache?
 */
async function findOrCreateComponent(
  testName = defaultTestName,
  statusPage = defaultStatusPage,
  useCache = true,
) {
  if (!statusPage) return null;

  if (useCache) {
    const cachedComponent = findComponentCache(testName);
    if (cachedComponent) return cachedComponent;
  }

  const components = await statusPage.getComponents();

  fs.writeFileSync(`${__dirname}/../components-cache.json`, JSON.stringify(components));

  const component = components.find(({ name }) => name === testName);
  if (!component) {
    const { data } = await statusPage.createComponent(testName);
    return data;
  }
  return component;
}

module.exports = async (...args) => {
  try {
    const data = await findOrCreateComponent(...args);
    return data;
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(`Não foi possível capturar o componente de teste: ${e.message}`);
    return null;
  }
};
