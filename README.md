# info-validation@1.0.0
Valida se o processo do INFO da BIPBOP está funcionando de forma adequada.

## TL;DR

```sh
node index.js
```

## Installation
This package is provided in these module formats:

- Mocha

## Dependencies

- [bipbop-webservice](): 
- [bipbop-websocket](https://github.com/bipbop/bipbop-websocket): Connect to bipbop websocket endpoint.
- [human-interval](https://github.com/rschmukler/human-interval): Human readable time measurements
- [mocha](https://github.com/mochajs/mocha): simple, flexible, fun test framework

## Dev Dependencies

None

## License
[GPLv3]()
